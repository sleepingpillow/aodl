using System;
using System.Drawing;
using System.IO;
using AODL.IO;
using SizeConverter = AODL.Document.Helper.SizeConverter;

namespace AODL.Document.Content.Draw
{
    public class ImageInfo
    {
        public ImageInfo(IFile file, string svgWidth, string svgHeight)
        {
            Image image = null;
            Graphics graphics = null;
            try {
                try {
                    using (Stream fileStream = file.OpenRead()) { 
                        image = Image.FromStream(fileStream);
                    }
                } catch (OutOfMemoryException e) {
                    throw new AODLGraphicException(string.Format("file '{0}' contains an unrecognized image", file), e);
                }

                try {
                    graphics = Graphics.FromImage(image);
                } catch (Exception e) {
                    throw new AODLGraphicException(string.Format(
                                                       "file '{0}' contains an unrecognized image. I could not" +
                                                       " create graphics object from image {1}", file, image),
                                                   e);
                }

                WidthInPixel = image.Width;
                HeightInPixel = image.Height;
                DPI_X = (int)graphics.DpiX;
                DPI_Y = (int)graphics.DpiY;
                MeasurementFormat = "cm";
                Width = SizeConverter.GetWidthInCm(image.Width, DPI_X);
                Height = SizeConverter.GetHeightInCm(image.Height, DPI_Y);
                if (svgHeight == null && svgWidth == null) {
                    SvgHeight = Height.ToString("F3") + MeasurementFormat;
                    if (SvgHeight.IndexOf(",", 0) > -1)
                        SvgHeight = SvgHeight.Replace(",", ".");
                    SvgWidth = Width.ToString("F3") + MeasurementFormat;
                    if (SvgWidth.IndexOf(",", 0) > -1)
                        SvgWidth = SvgWidth.Replace(",", ".");
                } else {
                    // This should only reached, if the file is loading by a importer.
                    MeasurementFormat = SizeConverter.IsCm(svgWidth) ? "cm" : "in";
                    SvgWidth = svgWidth;
                    SvgHeight = svgHeight;
                }

                Name = file.Name;
            } finally {
                if (image != null)
                    image.Dispose();
                if (graphics != null)
                    graphics.Dispose();
            }
        }

        public int WidthInPixel { get; }

        public int HeightInPixel { get; }

        public int DPI_X { get; }

        public int DPI_Y { get; }

        public string MeasurementFormat { get; }

        public double Width { get; }

        public double Height { get; }

        public string SvgWidth { get; }

        public string SvgHeight { get; }

        public string Name { get; }
    }
}